﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AmsDatabaseConnectionLibrary;
using AmsDatabaseConnectionLibrary.Enums;
using AmsDatabaseConnectionLibrary.Model.Common;
using DataMigrationTools;
using ExtensionsLibrary.Extensions;
using ObjectAnalysisProject.Extensions;
using OfficeLibrary;
using WindowsFormsLibrary.Extensions;
using WorkingTimeImportOfPart.Properties;

namespace WorkingTimeImportOfPart {
	/// <summary>
	/// インポートフォーム
	/// </summary>
	public partial class FormImport : FormImportDirectoryBase {
		#region コンストラクタ

		/// <summary>
		/// <see cref="FormImport"/> クラスの新しいインスタンスを初期化します。
		/// </summary>
		public FormImport() : base() {
			this.InitializeComponent();

			this.Text = "勤怠データ取込 (パート社員)";
			this.LogRowLimit = Settings.Default.LogRowLimit;
		}

		#endregion

		#region プロパティ

		#endregion

		#region イベントハンドラ

		#endregion

		#region メソッド

		#region 実行処理

		/// <summary>
		/// 実行処理の中核です。オーバーライドできます。
		/// </summary>
		protected override void RunCore() {
			var files = this.GetFileInfos();

			files.ForEach(source => {
				this.WriteLineMessage($"ファイル名：{source.FullName}");
				var xlsx = new ExcelManager(source.FullName);
				var sheetCount = xlsx.SheetCount;

				var rowTop = Settings.Default.ListTop;
				var rowLeft = Settings.Default.ListLeft;
				var rowCount = Settings.Default.ListRowCount;

				try {
					var data = xlsx.FetchValueFromSheet(sheet => {
						var userInfo = sheet.Cells[Settings.Default.従業員].Text.Split('/')
							.Select(v => v.Trim()).ToList();

						if (userInfo.Count != 3) {
							throw new ApplicationException("従業員情報が不正です。");
						}

						var idAndName = userInfo[2];
						var id = Regex.Replace(idAndName, @"[^0-9]", string.Empty);
						var name = idAndName.Replace(id, string.Empty).Trim();
						var org = userInfo[0];
						var ym = ToFiscalYearMonth(sheet.Cells[Settings.Default.月度].Value);

						var cell01 = sheet.Cells[Settings.Default.出勤].Text.ToNullableInt();
						var cell02 = sheet.Cells[Settings.Default.公休].Text.ToNullableInt();
						var cell03 = sheet.Cells[Settings.Default.年休].Text.Split('(')[0].Trim().ToNullableDouble();
						var cell04 = sheet.Cells[Settings.Default.特休].Text.ToNullableInt();
						var cell05 = sheet.Cells[Settings.Default.失年休].Text.Split('(')[0].Trim().ToNullableDouble();

						var rows = (
							from pos in Enumerable.Range(rowTop, rowCount)
							let day = sheet.Cells[pos, rowLeft + 1].Text.ToDateTime("MM/dd（ddd）")
							let schedule = sheet.Cells[pos, rowLeft + 3].Text
							let atte = ToAttendanceInfo(schedule)
							let typ = schedule.IsEmpty()
								? AttendanceTypes.Unknown
								: schedule.ToEnum<AttendanceTypes>(AttendanceTypes.出勤)
							select new {
								年月度 = ym,
								社員番号 = id,
								氏名 = name,
								勤務日 = day,
								勤怠 = typ,
								予定 = atte,
								所定時間 = sheet.Cells[pos, rowLeft + 7].Value.ToNullable(v => TimeSpan.FromHours(Convert.ToDouble(v))),
								出勤時間 = sheet.Cells[pos, rowLeft + 5].Value.ToNullable(v => v.ToString().Split(' ')[1].ToTimeSpan().Value),
								退勤時間 = sheet.Cells[pos, rowLeft + 6].Value.ToNullable(v => v.ToString().Split(' ')[1].ToTimeSpan().Value),
								休憩時間 = sheet.Cells[pos, rowLeft + 11].Value.ToNullable(v => TimeSpan.FromHours(Convert.ToDouble(v))),
								労働時間 = sheet.Cells[pos, rowLeft + 12].Value.ToNullable(v => TimeSpan.FromHours(Convert.ToDouble(v))),
							}
						).Where(row => row.勤務日.HasValue).ToList();

						return new {
							年月度 = ym,
							社員番号 = id,
							所属 = org,
							氏名 = name,
							出勤 = cell01,
							公休 = cell02,
							年休 = cell03,
							特休 = cell04,
							失年休 = cell05,
							一覧 = rows,
						};
					});

					if (data.年月度 == null) {
						throw new ApplicationException($"{nameof(data.年月度)}が空です。");
					}

					if (data.社員番号.IsEmpty()) {
						throw new ApplicationException($"{nameof(data.社員番号)}が空です。");
					}

					using (var db = new AmsDataClassesDataContext()) {
						if (!db.m_user.Any(u => u.syain_no == data.社員番号)) {
							throw new ApplicationException($"存在しない{nameof(data.社員番号)}です。: [{data.社員番号}]");
						}
					}

					this.WriteLineMessage($"{data.年月度.FiscalYear}/{data.年月度.FiscalMonth} [{data.社員番号}]{data.氏名}({data.所属})");

					if (!data.一覧.Any()) {
						throw new ApplicationException("入力データがありません。");
					}

					var now = DateTime.Now;
					var num = nameof(FormImport);
					var items = data.一覧.Select(d => new t_work_d {
						syain_no = d.社員番号,
						nendo = d.年月度.FiscalYear.ToString(),
						getsudo = d.年月度.FiscalMonth.ToString("00"),
						work_day = d.勤務日.Value,
						kintai_kbn_cd = d.勤怠.ToInt32().ToString("00"),
						standard_time = d.所定時間?.ToHourAndMinString(),

						kouban_in_time = d.予定?.access_in_time,
						kouban_out_time = d.予定?.access_out_time,
						kouban_rest_time = d.休憩時間?.ToHourAndMinString(),

						access_in_time = d.出勤時間?.ToHourAndMinString(),
						access_out_time = d.退勤時間?.ToHourAndMinString(),

						jisseki_in_time = d.出勤時間?.ToHourAndMinString(),
						jisseki_out_time = d.退勤時間?.ToHourAndMinString(),
						jisseki_rest_time = d.休憩時間?.ToHourAndMinString(),

						work_time = d.労働時間?.ToHourAndMinString(),

						kouban_approval_flg = "1",
						jisseki_approval_flg = "1",

						ins_date = now,
						ins_syain_no = num,
						upd_date = now,
						upd_syain_no = num,
					});

					var item = (
						from d in items
						group d by new {
							d.syain_no,
							d.nendo,
							d.getsudo,
						} into dd
						select new {
							dd.Key.syain_no,
							dd.Key.nendo,
							dd.Key.getsudo,
							所定時間累計 = new TimeSpan(dd.Sum(d => (d.standard_time.ToTimeSpan() ?? TimeSpan.Zero).Ticks)),
							労働時間累計 = new TimeSpan(dd.Sum(d => (d.standard_time.ToTimeSpan() ?? TimeSpan.Zero).Ticks)),
						}
					).FirstOrDefault();

					var mWork = new t_work_m {
						syain_no = item.syain_no,
						nendo = item.nendo,
						getsudo = item.getsudo,

						total_work_time = item.労働時間累計.ToHourAndMinString(),
						total_standard_time = item.所定時間累計.ToHourAndMinString(),
						used_public_holiday = data.公休 ?? 0,
						used_paid_holiday = Convert.ToInt32(data.年休 ?? 0.0),
						used_lost_paid_holiday = Convert.ToInt32(data.失年休 ?? 0.0),
						kouban_approval_flg = "1",
						modify_count_after_approval = 0,
						closing_flg = "1",

						ins_date = now,
						ins_syain_no = num,
						upd_date = now,
						upd_syain_no = num,
					};

					using (var db = new AmsDataClassesDataContext()) {
						db.t_work_d.InsertAllOnSubmit(items);

						db.t_work_m.InsertOnSubmit(mWork);

						db.SubmitChanges();
					}

					this.WriteLineMessage($"[{data.社員番号}]{data.氏名} データを登録しました。");
				} catch (ArgumentException ex) {
					this.WriteLineMessage(ex.Message);
				} catch (InvalidOperationException ex) {
					this.WriteLineMessage(ex.Message);
				} catch (ApplicationException ex) {
					this.WriteLineMessage(ex.Message);
				} catch (SqlException ex) when (ex.Message.HasString("PRIMARY KEY")) {
					this.WriteLineMessage(ex.Message);
				} catch (Exception ex) {
					this.WriteException(ex);
				}
			});

			this.ShowDirectoryInfo();
		}

		private static FiscalYearMonth ToFiscalYearMonth(object value) {
			try {
				var dt = Convert.ToDateTime(value);

				return new FiscalYearMonth(dt.Year, dt.Month);
			} catch (Exception) {
				return null;
			}
		}

		private static AttendanceInfo ToAttendanceInfo(string schedule) {
			try {
				var sp = schedule.Mid(2).Split('~');
				return new AttendanceInfo(sp[0], sp[1]);
			} catch (Exception) {
				return null;
			}
		}

		private IEnumerable<FileInfo> GetFileInfos() {
			var rows = this.Rows;
			var files = (
				from row in rows
				let fullPath = row.Cells[nameof(FileInfo.FullName)].Value.ToString()
				select new FileInfo(fullPath)
			);
			return files;
		}

		#endregion

		#region DataSource 生成

		/// <summary>
		/// ディレクトリ情報から DataGridView に表示する DataSource を生成します。
		/// </summary>
		/// <param name="info">ディレクトリ情報</param>
		/// <returns>生成した DataSource を返します。</returns>
		protected override DataTable CreateDataSource(DirectoryInfo info) {
			var dataSource = (
				from f in info.GetFileInfos("*.xlsx")
				select new {
					f.Name,
					f.FullName,
				}
			).ToDataTable();

			return dataSource;
		}

		#endregion

		#endregion
	}
}
