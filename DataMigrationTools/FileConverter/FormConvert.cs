﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using DataMigrationTools;
using ExtensionsLibrary.Extensions;
using FileConverter.Extensions;
using FileConverter.Properties;
using Microsoft.Office.Interop.Excel;
using ObjectAnalysisProject.Extensions;
using WindowsFormsLibrary.Extensions;

namespace FileConverter {
	/// <summary>
	/// ファイル変換フォーム
	/// </summary>
	public partial class FormConvert : FormImportDirectoryBase {
		#region コンストラクタ

		/// <summary>
		/// <see cref="FormConvert"/> クラスの新しいインスタンスを初期化します。
		/// </summary>
		public FormConvert() : base() {
			this.InitializeComponent();

			this.Text = "Excel ファイル変換";
			this.LogRowLimit = Settings.Default.LogRowLimit;
		}

		#endregion

		#region プロパティ

		#endregion

		#region イベントハンドラ

		#endregion

		#region メソッド

		#region 実行処理

		/// <summary>
		/// 実行処理の中核です。オーバーライドできます。
		/// </summary>
		protected override void RunCore() {
			var files = this.GetFileInfos();

			files.ForEach(source => {
				try {
					var destination = ConvertToXlsx(source);

					this.WriteLineMessage($"ファイルを変換しました。 : {destination.Name}");

					if (!Settings.Default.LeavingOriginal) {
						source.Delete();
						this.WriteLineMessage($"元ファイルを削除しました。 : {source.Name}");
					}
				} catch (ArgumentException ex) {
					this.WriteLineMessage(ex.Message);
				} catch (InvalidOperationException ex) {
					this.WriteLineMessage(ex.Message);
				} catch (ApplicationException ex) {
					this.WriteLineMessage(ex.Message);
				} catch (Exception ex) {
					this.WriteLineMessage($"ファイルの変換に失敗しました。: {ex.Message}");
				}
			});

			this.ShowDirectoryInfo();
		}

		/// <summary>
		/// 旧式のExcelファイルを変換(.xlsx)します。
		/// </summary>
		/// <param name="source">ファイル情報</param>
		/// <returns>変換後のファイル情報</returns>
		private static FileInfo ConvertToXlsx(FileInfo source) {
			var destination = new FileInfo($@"{source.FullName}x");

			source.OpenExcel(workbook => {
				workbook.SaveAs(destination.FullName, FileFormat: XlFileFormat.xlOpenXMLWorkbook);
			});

			return destination;
		}

		private IEnumerable<FileInfo> GetFileInfos() {
			var rows = this.Rows;
			var files = (
				from row in rows
				let fullPath = row.Cells[nameof(FileInfo.FullName)].Value.ToString()
				select new FileInfo(fullPath)
			);
			return files;
		}

		#endregion

		#region DataSource 生成

		/// <summary>
		/// ディレクトリ情報から DataGridView に表示する DataSource を生成します。
		/// </summary>
		/// <param name="info">ディレクトリ情報</param>
		/// <returns>生成した DataSource を返します。</returns>
		protected override System.Data.DataTable CreateDataSource(DirectoryInfo info) {
			var dataSource = (
				from f in info.GetFileInfos(true, ".xlsx")
				select new {
					f.Name,
					f.FullName,
				}
			).ToDataTable();

			return dataSource;
		}

		#endregion

		#endregion
	}
}
