﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;

namespace FileConverter.Extensions {
	/// <summary>
	/// FileInfo を拡張するメソッドを提供します。
	/// </summary>
	public static partial class FileInfoExtension {
		#region メソッド

		/// <summary>
		/// Excel ファイルとしてファイルを開きます。
		/// </summary>
		/// <param name="this">FileInfo</param>
		/// <param name="operate">ワークブックを操作する関数</param>
		/// <remarks>
		/// operate メソッド内で COM オブジェクトの参照解放処理をしっかりしないとプロセスが残ります。
		/// </remarks>
		public static void OpenExcel(this FileInfo @this, Action<Workbook> operate) {
			var app = new Application();
			var workbooks = app.Workbooks;
			var workbook = workbooks.Open(@this.FullName);
			try {
				operate?.Invoke(workbook);
			} finally {
				workbook.Close();

				Marshal.ReleaseComObject(workbook);
				workbook = null;

				Marshal.ReleaseComObject(workbooks);
				workbooks = null;

				Collect();

				app.Quit();
				Marshal.ReleaseComObject(app);
				app = null;
				Collect();
			}
		}

		private static void Collect() {
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
		}

		#endregion
	}
}
